package com.example.savingstoolkit

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

/**
 * SavingsToolkitCacheImpl
 *
 * @author Nam Le on 2023-05-15.
 */
internal class SavingsToolkitCacheImpl @Inject constructor(): SavingsToolkitCache {

    private var storedValue  = MutableLiveData(-1)

    override fun storeValue(value: Int) {
        storedValue.value = value
    }

    override fun getValue() = storedValue
}
