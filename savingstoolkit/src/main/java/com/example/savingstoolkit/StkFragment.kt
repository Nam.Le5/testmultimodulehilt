package com.example.savingstoolkit

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.savingstoolkit.databinding.StkFragmentBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.random.Random

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class StkFragment : Fragment() {

    @Inject lateinit var savingsToolkitCache: SavingsToolkitCache

    private var _binding: StkFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = StkFragmentBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textviewSecond.text = "Welcome to stk feature fragment\n\nCache instance:\n$savingsToolkitCache"
        binding.buttonSecond.setOnClickListener {
            val value = Random.nextInt(0, 10)
            savingsToolkitCache.storeValue(value)
            Snackbar.make(it.context, it, "Storing value: $value", Snackbar.LENGTH_LONG ).show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
