package com.example.savingstoolkit

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.savingstoolkit.databinding.StkFragmentLandingBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class StkLandingFragment : Fragment() {

    @Inject
    lateinit var savingsToolkitCache: SavingsToolkitCache
    private var _binding: StkFragmentLandingBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = StkFragmentLandingBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savingsToolkitCache.getValue().observe(viewLifecycleOwner){
            if(it == -1){
                binding.textviewFirst.text = "STK UI with NO cached value\n\nCache instance:\n$savingsToolkitCache"
            }else{
                binding.textviewFirst.text = "STK UI with cached value: $it\n\nCache instance:\n$savingsToolkitCache"
            }
        }
        binding.root.setOnClickListener {
            activity?.startActivity(
                Intent(context, SavingsToolkitActivity::class.java)
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
