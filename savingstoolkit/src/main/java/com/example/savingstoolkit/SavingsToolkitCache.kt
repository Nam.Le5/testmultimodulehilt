package com.example.savingstoolkit

import androidx.lifecycle.LiveData

/**
 * SavingsToolkitCache
 *
 * @author Nam Le on 2023-05-15.
 */
interface SavingsToolkitCache {

    fun storeValue(value: Int)

    fun getValue(): LiveData<Int>
}
