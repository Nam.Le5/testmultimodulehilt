package com.example.savingstoolkit.di

import com.example.savingstoolkit.SavingsToolkitCache
import com.example.savingstoolkit.SavingsToolkitCacheImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped

/**
 * LoginModule
 *
 * @author Nam Le on 2023-05-15.
 */
@Module
@InstallIn(ActivityComponent::class)
internal interface StkModule {

    @Binds
    @ActivityScoped
    fun bindLoginCache(
        repositoryImpl: SavingsToolkitCacheImpl
    ): SavingsToolkitCache
}
