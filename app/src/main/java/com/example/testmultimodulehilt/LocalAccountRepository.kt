package com.example.testmultimodulehilt

/**
 * LocalAccountRepository
 *
 * @author Nam Le on 2023-05-11.
 */
interface LocalAccountRepository {

    fun getAccount(): List<String>
}
