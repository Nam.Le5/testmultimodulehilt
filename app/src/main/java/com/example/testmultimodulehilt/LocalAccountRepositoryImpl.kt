package com.example.testmultimodulehilt

import javax.inject.Inject

/**
 * LocalAccountRepositoryImpl
 *
 * @author Nam Le on 2023-05-11.
 */
class LocalAccountRepositoryImpl @Inject constructor(): LocalAccountRepository {
    override fun getAccount(): List<String> = listOf("local account 1", "local account 2")
}
