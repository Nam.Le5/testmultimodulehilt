package com.example.testmultimodulehilt

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * MyApplication
 *
 * @author Nam Le on 2023-05-11.
 */
@HiltAndroidApp
class MyApplication : Application() {
}
