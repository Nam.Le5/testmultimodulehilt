package com.example.testmultimodulehilt.di

import com.example.testmultimodulehilt.LocalAccountRepository
import com.example.testmultimodulehilt.LocalAccountRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

/**
 * LocalModule
 *
 * @author Nam Le on 2023-05-11.
 */
@Module
@InstallIn(ActivityComponent::class)
abstract class LocalModule {

    @Binds
    abstract fun bindAnalyticsService(
        repositoryImpl: LocalAccountRepositoryImpl
    ): LocalAccountRepository
}
