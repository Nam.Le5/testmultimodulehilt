package com.example.testmultimodulehilt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.savingstoolkit.SavingsToolkitCache
import com.example.testmultimodulehilt.databinding.FragmentThirdBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.random.Random

/**
 * ThirdFragment
 *
 * @author Nam Le on 2023-05-16.
 */
@AndroidEntryPoint
class ThirdFragment : Fragment() {

    @Inject
    lateinit var cache: SavingsToolkitCache
    private var _binding: FragmentThirdBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textview.text = "Third Fragment on Main\n\nCache instance:\n$cache"
        binding.button.setOnClickListener {
            val value = Random.nextInt(0, 10)
            cache.storeValue(value)
            Snackbar.make(it.context, it, "Storing value: $value", Snackbar.LENGTH_LONG ).show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
