## About
This repository is a playground to test Hilt with complex scenarios like Savings Toolkit. Unlike other modules,
Savings Toolkit (STK) starts its lifecycle on a Fragment that belongs to the MainApp (Account Overview), and any further interactions with
that Fragment will start Savings Toolkit Activity. When we exits the initial fragment, its lifecycle needs to be stopped,
and its component needs to be destroyed. 
Notice how the cache instance hashcode are different when you're in Savings Toolkit Activity
and in the Account Overview fragment? That's the problem we're talking about.
Ideally, from Account Overview fragment to SavingsToolkit Activity, we should have the same hashcode. When we exits the
Account Overview fragment by pressing back button, then go to the Account Overview fragment again, the hashcode need
to be changed (because component supposed to be destroyed, and re-create)

## Branches
Checkout some of the below branches to play with different solutions. Each branch is a solution.
### singleton
A solution in which we scope the SavingsToolkitModule to AppComponent. Not ideal for two reasons:
- If the whole app will follow the pattern and its a waste of resources: long start-up time, battery life, memories, etc.
- Modules may have their own coroutineScopes to make service calls like STK, and we want to be able 
to cancel the scope when the components get destroyed. However, the AppComponent never gets destroyed.
- This solution exposes SavingsToolkit internal classes to the whole app.

### wip-custom-component
Not a fully implemented custom components and component managers. It is a transition to a full blow 
solution where everything is implemented. Understanding the code in this solution will help a lot when
working with the **custom-component** branch. This branch features include:
- A custom StkComponent (no custom data or cleaning up yet)
- A custom StkScope so we can scope the Cache to the custom component.
Because it is a work-in-progress it does not automatically destroy the fragment.

### custom-component
A fully custom component with below features:
- A custom StkComponent where we can attach custom data to the component, do some clean up before
the component gets destroyed. The cleaning up works help cancel coroutine scope for all Savings 
Toolkit service calls.
- A custom StkScope, so we can scope the Cache to the custom component.
- The component gets destroyed when we exit the initial fragments.

This solution has a downside, i.e. we have to implement the injection ourselves.

### hilt-and-dagger-together
This solution tries to avoid the manual injection that happens on **custom-component** branch. 
In this solution, we let the MainApp provide another repository (AccountRepository), so we
have dependencies on the SavingsToolkit component. We will then use regular Dagger component not
hilt. This approach has all the benefit of the other approach + automatically injection.

However, this solution has downsides too.
- The main app is Hilt component, so all the dependencies need to be inject into the Application class. If all features
follow this approach the Application class will get really big. Prior to Hilt, we don't have this problem because
the AppComponent is responsible for that. With Hilt, the component is generated automatically, so we can't customize it.
