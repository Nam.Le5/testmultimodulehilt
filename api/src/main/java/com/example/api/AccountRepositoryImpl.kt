package com.example.api

import javax.inject.Inject

/**
 * AccountRepositoryImpl
 *
 * @author Nam Le on 2023-05-11.
 */
internal class AccountRepositoryImpl @Inject constructor() : AccountRepository {

    override fun getAccount(): List<String> = listOf("Api Account 1", "Api Account 2")
}
