package com.example.api

/**
 * LogRocketRepository
 *
 * @author Nam Le on 2023-05-11.
 */
interface AccountRepository {
    fun getAccount() : List<String>
}
