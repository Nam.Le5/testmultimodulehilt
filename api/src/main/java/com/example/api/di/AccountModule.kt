package com.example.api.di

import com.example.api.AccountRepository
import com.example.api.AccountRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * AccountModule
 *
 * @author Nam Le on 2023-05-11.
 */
@Module
@InstallIn(ActivityComponent::class)
internal interface AccountModule {

    @Binds
    @ActivityScoped
     fun bindAccountRepository(
        repositoryImpl: AccountRepositoryImpl
    ): AccountRepository

}
